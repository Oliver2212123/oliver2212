package operativni;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class vtora {
	public static void main(String[] args) throws IOException {

		InputStream  in = new FileInputStream("izvor.txt");
		
		OutputStream out = new FileOutputStream("des2.txt");
		
		try
		{
			int c;
			ArrayList<Integer> lista = new ArrayList<>();
			while((c=in.read())!=-1)
			{
				lista.add(c);
			}
			Collections.reverse(lista);
			for(Integer con : lista)
			{
				out.write(con);
			}
		}
		
		finally
		{
			if(in !=null)
			in.close();
			
			if(out!=null)
				out.close();
		}
		
		
		
	}
}
