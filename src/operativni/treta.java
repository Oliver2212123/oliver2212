package operativni;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class treta {
	public static void main(String[] args) throws IOException {
		BufferedReader br = null;
		BufferedWriter bw = null;
		try
		{
			br = new BufferedReader(new FileReader("izvor.txt"));
		    bw = new BufferedWriter(new FileWriter(new File("de.txt")));
		    ArrayList<Integer> text = new ArrayList<>();
		    int c;
		    while ((c=br.read())!=-1)
		    {
		    	text.add(c);
		    }
		    Collections.reverse(text);
		    for(int i=0;i<text.size();i++)
		    {
		    	bw.write(text.get(i));
		    }
		}
		finally
		{
			if(bw!=null)
			{
				bw.close();
			}
			if(br !=null)
			{
				br.close();
			}
		}
		
	}

}
